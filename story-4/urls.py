from django.urls import path
from . import views

urlpatterns = [
    path('',views.home,name='Homepage'),
    path('profile/', views.basic_info, name='basic_info'),
    path('profile/education', views.education, name='education'),
    path('profile/organization', views.organization, name='organization'),
    path('profile/awards', views.awards, name = 'awards'),
    path('profile/skills', views.skills, name = 'skills'),
    path('projects/', views.projects, name='projects'),
    path('contact/', views.contact, name='contact'),
]
