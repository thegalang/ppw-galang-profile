from django.forms import ModelForm
from django import forms
from .models import Activity
class NewActivityForm(ModelForm):

	
	class Meta:
		model = Activity
		fields = ['name', 'date', 'time', 'location', 'activity']
		widgets = {
			'date': forms.DateInput(format=('%m/%d/%y'), attrs={'type' : 'date'}),
			'time': forms.TimeInput(format=('%H:%M'), attrs={'type' : 'time'}),
		}
		
	def __init__(self, *args, **kwargs):
		super(NewActivityForm, self).__init__(*args, **kwargs)

		fields = ['name', 'date', 'time', 'location', 'activity']

		# adding classes
		for field in fields:
			self.fields[field].widget.attrs.update({ 
				'id' : field,
				'class' : 'form-control',
			})
			self.fields[field].required = True

		self.fields['name'].widget.attrs.update({'placeholder': 'Enter activity name'})

		self.fields['location'].widget.attrs.update({'placeholder' : 'Enter activity location'})

		
