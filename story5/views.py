from django.shortcuts import render,redirect
from .forms import NewActivityForm
from .models import Activity

# homepage
def home(request):
    return render(request,'story5/Homepage.html',{})

# ------- profile story5 ------
def basic_info(request):
	dataDict = {
		"profile_info": [(True, "", 
						"""Full Name		: Galangkangin Gotera
						   Date of Birth	: February 29, 2000
						   Occupation		: Computer Science Student
						   Location			: Depok, Jawa Barat, Indonesia
						   Blog				: jurnalgalang.com
						   Email			: galangkangingotera@hotmail.com""")]
	}
	return render(request, 'story5/Profile.html', dataDict)


def education(request):
	dataDict = {
		"profile_info": [(True, "Data Science Intern", "Ovo Big Data Team\nJune 2019 - August 2019"),
						 (False, "AI Engineer Intern", "GDP Labs AI Research\nDecember 2018 - January 2019"),
						 (False, "Computer Science Undergraduate", "University of Indonesia\n2018 - Present")]
	}
	return render(request, 'story5/Profile.html', dataDict)

def organization(request):
	dataDict = {
		"profile_info": [(True, "BEM Fasilkom UI", "Staff of Scientific Department\nFebruary 2019 - now"),
						 (False, "RISTEK Fasilkom UI", "Data Science Junior Member\nMarch 2019 - now"),
						 (False, "Pelatnas TOKI", "Scientific Committee\nFebruary 2019 - now")]
	}
	return render(request, 'story5/Profile.html', dataDict)

def awards(request):
	dataDict = {
		"profile_info": [(True, "Bronze Medal", "Asia ICPC Jakarta Regional 2018"),
						 (False, "Gold Medal", "OSN Informatics 2017"),
						 (False, "Third Place", "National Statistics Competition ITS 2017")]
	}
	return render(request, 'story5/Profile.html', dataDict)

def skills(request):
	dataDict = {
		"profile_info": [(True, "Algorithms and Data Structure", "In-depth knowledge of various advanced algorithms for runtime optimization"),
						 (False, "Python, C++, and Java", "Able to code fluently in these programming languages"),
						 (False, "Mathematics and Statistics", "Possess mathematical background for analyzing data"),
						 (False, "Data Science using Python", "Comprehensive knowledge of DS tools used in python such as pandas and matplotlib")]
	}
	return render(request, 'story5/Profile.html', dataDict)

# ------- end profile story5 ------

# projects

def projects(request):
	dataDict = {

		"project_info": [(True, "jurnalgalang.com", "assets/JurnalGalang.png", "A personal blog just to record my current status in life"),
						 (False, "Face Anti Spoofing with IQA", "assets/IQA_Paper.png", "Trying to understand and implement cutting-edge IQA for image spoofing attack")]
	}

	return render(request, "story5/Projects.html", dataDict);

# ------- end projects -------

# contact

def contact(request):

	return render(request, "story5/Contact-Me.html")

# ------- end contact me

# begin activity

def activity(request):


	if(request.method == "POST"):
		form = NewActivityForm(request.POST)

		# kalau valid, di save
		if(form.is_valid()):
			form.save()

		# redirect biar ga submit ulang form
		return redirect("activity")

	# valid ga valid, kembali ke halaman ini
	form = NewActivityForm()
	activity_data = Activity.objects.all()
	return render(request, 'story5/Activity.html', {'form': form, 'activity_data': activity_data})

def delete_activity(request, obj_pk = None):
	if(obj_pk is not None):
		activity = Activity.objects.get(pk=obj_pk).delete()

	return redirect("activity")