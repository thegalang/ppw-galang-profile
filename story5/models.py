from django.db import models

TYPE_CHOICES = [
	("Academic", "Academic"), 
	("Organization", "Organization"), 
	("Group Work", "Group Work"), 
	("Recreation","Recreation"),
]

class Activity(models.Model):
	name = models.CharField(max_length=12)
	date = models.DateField()
	time = models.TimeField(default='00:00')
	location = models.CharField(max_length = 10)
	activity = models.CharField(max_length = 15, choices = TYPE_CHOICES, default="Academic")
	

	def __str__(self):
		return self.name


